class HomeController < ApplicationController
    skip_before_action :verify_authenticity_token

	def index
		@webhooks = Webhook.all
	end

	def callback
		render :nothing => true, :status => 200
		puts "GOT CALLBACK!"
		save_webhook_params(params)
	end

    private
    
	def save_webhook_params(params)
		@params = params

        hook_id = @params[:issue][:id]
		puts "ID #{hook_id}"
		
		action = @params[:action]
		puts "Ação #{action}"

		number = @params[:issue][:number]
		puts "Numero #{[:number]}"
		
		name = @params[:repository][:name]
        puts "Projeto #{name}"
        
        full_name = @params[:repository][:full_name]
        puts "Nome Completo #{full_name}"

		created_at = @params[:issue][:created_at]
		puts "Criado #{created_at}"


		@webhook = Webhook.new(hook_id: hook_id, action: action, number: number, name: name, full_name: full_name,created_at: created_at)
		@webhook.save!
	end
end
