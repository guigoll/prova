class Api::V1::WebhookController < ApplicationController

	def index
		webhooks = Webhook.all
		render json: {Webhook: webhooks}, status: 200
	end

	def show
		webhook = Webhook.find(params[:id])
		render json: webhook, status: 200
	end

end
