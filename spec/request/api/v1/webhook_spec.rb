require 'rails_helper'

RSpec.describe 'API WebHook', type: :request do

  before { host! 'api.octo.test'}

  let!(:webhook){ FactoryBot.create(:webhook)}

  let(:headers) do
    {
      'Accept' => 'application/vnd.octo.v1',
      'Content-type' => Mime[:json].to_s      
    }
  end

  describe 'GET /webhook' do
    before do
      FactoryBot.create_list(:webhook, 5 )
      get '/webhook', params:{}, headers: headers
    end

    it 'return status code 200'  do
       expect(response).to have_http_status(200)
    end
  end

  describe 'GET /webhook:id' do
    let(:webhook){ FactoryBot.create(:webhook)}
    before { get "/webhook/#{webhook.id}",  params: {}, headers: headers}

    it 'return status code 200'  do
       expect(response).to have_http_status(200)
    end
  end

end