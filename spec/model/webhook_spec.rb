require 'rails_helper'

RSpec.describe Webhook, type: :model do
  let(:team){ FactoryBot.build(:webhook) }

  # verifica se os campos ainda existe no database
  it{ is_expected.to respond_to(:name)}
  it{ is_expected.to respond_to(:action)}
  it{ is_expected.to respond_to(:number)}
  it{ is_expected.to respond_to(:full_name)}
  it{ is_expected.to respond_to(:create_at)}
end