FactoryBot.define do
    factory :webhook do
      name "task-manager-api"
      full_name "guigo/task-manager-api"
      create_at "2018-07-02 00:00:00"
    end
  end