require 'api_version_constraint'

Rails.application.routes.draw do

  post '/callback', to: 'home#callback', constraints: { format: 'json' }
  get '/', to: 'home#index'

  namespace :api, default:{ format: :json}, constraints: {subdomain: 'api'}, path: '/' do
    namespace :v1, path: '/', constraints: ApiVersionConstraint.new(version: 1, default: true) do
      resources :webhook, only:[:index, :show ]
    end
  end
  
end
