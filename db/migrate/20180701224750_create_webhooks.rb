class CreateWebhooks < ActiveRecord::Migration
  def change
    create_table :webhooks do |t|
      t.string :hook_id
      t.integer :number
      t.string :action
      t.string :name
      t.string :full_name
      t.string :create_at

      t.timestamps null: false
    end
  end
end
